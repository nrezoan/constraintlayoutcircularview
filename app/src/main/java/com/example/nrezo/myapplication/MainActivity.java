package com.example.nrezo.myapplication;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.animation.ValueAnimator;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    ImageView imgPointer, imgClock;
    ValueAnimator clockAnimator;
    TextView one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgPointer = (ImageView) findViewById(R.id.imgPointer);
        imgClock = (ImageView) findViewById(R.id.imgClock);



        imgClock.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),getRand());
                clockAnimator.start();

            }
        });
        one = findViewById(R.id.one);
        one.setOnClickListener(this);
        two = findViewById(R.id.two);
        two.setOnClickListener(this);
        three = findViewById(R.id.three);
        three.setOnClickListener(this);
        four = findViewById(R.id.four);
        four.setOnClickListener(this);
        five = findViewById(R.id.five);
        five.setOnClickListener(this);
        six = findViewById(R.id.six);
        six.setOnClickListener(this);
        seven = findViewById(R.id.seven);
        seven.setOnClickListener(this);
        eight = findViewById(R.id.eight);
        eight.setOnClickListener(this);
        nine = findViewById(R.id.nine);
        nine.setOnClickListener(this);
        ten = findViewById(R.id.ten);
        ten.setOnClickListener(this);
        eleven = findViewById(R.id.eleven);
        eleven.setOnClickListener(this);
        twelve = findViewById(R.id.twelve);
        twelve.setOnClickListener(this);


    }


    private ValueAnimator animatePointer(long orbitDuration, int x) {
        Toast.makeText(this, ""+(x/30), Toast.LENGTH_SHORT).show();
        ValueAnimator anim = ValueAnimator.ofInt(0, x);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) imgPointer.getLayoutParams();
                layoutParams.circleAngle = val;
                imgPointer.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(orbitDuration);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatMode(ValueAnimator.RESTART);
        anim.setRepeatCount(0);
        return anim;
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onResume() {
        super.onResume();
        if (clockAnimator != null) {
            if (clockAnimator.isPaused()) {
                clockAnimator.resume();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onPause() {
        super.onPause();
        if (clockAnimator.isRunning()) {
            clockAnimator.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (clockAnimator.isRunning()) {
            clockAnimator.cancel();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.one:
                // do your code
                Toast.makeText(this, ""+"Pressed One", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),30);
                clockAnimator.start();
                break;

            case R.id.two:
                // do your code
                Toast.makeText(this, ""+"Pressed Two", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),60);
                clockAnimator.start();
                break;
            case R.id.three:
                // do your code
                Toast.makeText(this, ""+"Pressed three", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),90);
                clockAnimator.start();
                break;
            case R.id.four:
                // do your code
                Toast.makeText(this, ""+"Pressed four", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),120);
                clockAnimator.start();
                break;
            case R.id.five:
                // do your code
                Toast.makeText(this, ""+"Pressed five", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),150);
                clockAnimator.start();
                break;
            case R.id.six:
                // do your code
                Toast.makeText(this, ""+"Pressed six", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),180);
                clockAnimator.start();
                break;
            case R.id.seven:
                // do your code
                Toast.makeText(this, ""+"Pressed seven", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),210);
                clockAnimator.start();
                break;
            case R.id.eight:
                // do your code
                Toast.makeText(this, ""+"Pressed eight", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),240);
                clockAnimator.start();
                break;
            case R.id.nine:
                // do your code
                Toast.makeText(this, ""+"Pressed Nine", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),270);
                clockAnimator.start();
                break;
            case R.id.ten:
                // do your code
                Toast.makeText(this, ""+"Pressed Ten", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),300);
                clockAnimator.start();
                break;
            case R.id.eleven:
                // do your code
                Toast.makeText(this, ""+"Pressed Eleven", Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),330);
                clockAnimator.start();
                break;
            case R.id.twelve:
                // do your code
                Toast.makeText(this, ""+getRand(), Toast.LENGTH_SHORT).show();
                clockAnimator = animatePointer(TimeUnit.SECONDS.toMillis(1),0);
                clockAnimator.start();
                break;
            default:
                break;
        }

    }

    protected int getRand(){
        Random rand = new Random();

        return rand.nextInt(12)*30;
    }
}
